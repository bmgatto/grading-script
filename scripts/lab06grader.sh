#!/bin/bash

function run_script {
#    for input_num in $(ls input_$tasknum* | cut -d"." -f2)
#    do
#    python3 $script < input_$tasknum.$input_num 2> /dev/null 1> tmpfile
#    student_output=tmpfile
#    solution_output=solution_$tasknum.$input_num
#    echo ""
#    if cmp -s $student_output $solution_output
#    then
#        echo "The output is correct"
#        head $student_output
#    else
#        echo "$(tput bold; tput setaf 1) Incorrect Output $(tput sgr0)"
#        pr -mt $student_output $solution_output
#    fi
#    echo ""
#    done


    echo ""
    echo -e "$1" | python3 $script | head
    echo ""

}


ASSIGNMENT=lab06
NUMTASKS=5
EXT=py

student_list=$(ls | grep "[a-Z0-9\-]*.$EXT$" | cut -d'-' -f3 | cut -d'.' -f1 | sort -u)


if [[ $1 != '' ]]
then
    student_list=$1
fi


for student in $student_list
do
    clear
    for tasknum in $(seq -w 01 $NUMTASKS)
    do
        task="task$tasknum"
        script=$ASSIGNMENT-$task-$student.$EXT
        echo "#################################"
        echo "# $script $ASSIGNMENT $task $student"
        echo "#################################"
        echo "Script Contents:"
        echo ""
        if [ -f $script ]
        then
            cat $script
            echo ""
        else
            echo "$(tput bold; tput setaf 1) No submission $(tput sgr0)"
            echo ""
            continue
        fi

        echo "Script output:"
        if [[ $task == task01 ]]
        then

            run_script "30, 35, 30, 35, 40, 40\n80, 80, 85, 80, 90, 90, 100"
            run_script "0, 0, 0, 0, 0, 0\n0, 0, 0, 0, 0, 0, 0"
        fi

        echo "Script output:"
        if [[ $task == task02 ]]
        then
            run_script "add 1 2 3 4\nsub 20 30 100 50\nmul 60 7\ndiv 10 5\nexp 3 2\nmod 3 12"
        fi

        if [[ $task == task03 ]]
        then
            run_script "6\n6\n6\n20"
        fi

        if [[ $task == task04 ]]
        then
            run_script 
        fi

        if [[ $task == task05 ]]
        then
            run_script "Todd\nFrankie\nHomer\n742 Evergreen Terrace\nHomer"
        fi

    done
    read -p "You are viewing $student. Press enter to view the next student"
done
