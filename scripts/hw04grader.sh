#!/bin/bash



for student in $(ls | grep "[a-Z0-9\-]*.awk$" | cut -d'-' -f3 | cut -d'.' -f1 | sort -u)
do
    clear
    for tasknum in {01..10}
    do
        assignment=hw04
        task="task$tasknum"
        asurite=$student
        echo "#################################"
        echo "# $assignment $task $asurite"
        echo "#################################"
        echo "Script Contents:"
        echo ""
        if [ -f $assignment-$task-$asurite* ]
        then
            cat *$assignment-$task-$asurite*
        else
            echo "$(tput bold; tput setaf 1) No submission"
        fi
        echo ""


        echo "Script output:"
        if [[ $task == task0[0-4] ]]
        then
            IF=hw0401.csv
            studentScript=$(awk -f $assignment-$task-$asurite* $IF 2> /dev/null)
            sdevilScript=$(cat $assignment-$task-sdevil*)
            echo ""
            if [ "$studentScript" = "$sdevilScript" ]
            then
                echo "The output is correct"
                awk -f $assignment-$task-$asurite* $IF | head
            else
                echo "$(tput bold; tput setaf 1) Incorrect Output $(tput sgr0)"
                awk -f $assignment-$task-$asurite* $IF | head
            fi
            echo ""
        fi

        if [[ $task == task0[5-6] ]]
        then
            IF=hw0405.csv
            studentScript=$(awk -f $assignment-$task-$asurite* $IF 2> /dev/null)
            sdevilScript=$(cat $assignment-$task-sdevil*)
            echo ""
            if [ "$studentScript" = "$sdevilScript" ]
            then
                echo "The output is correct"
                awk -f $assignment-$task-$asurite* $IF | head
            else
                echo "$(tput bold; tput setaf 1) Incorrect Output $(tput sgr0)"
                awk -f $assignment-$task-$asurite* $IF | head
            fi
            echo ""
        fi
 
        if [[ $task == task07 ]]
        then
            IF=hw0407.csv
            studentScript=$(awk -f $assignment-$task-$asurite* $IF 2> /dev/null)
            sdevilScript=$(cat $assignment-$task-sdevil*)
            echo ""
            if [ "$studentScript" = "$sdevilScript" ]
            then
                echo "The output is correct"
                awk -f $assignment-$task-$asurite* $IF | head
            else
                echo "$(tput bold; tput setaf 1) Incorrect Output $(tput sgr0)"
                awk -f $assignment-$task-$asurite* $IF | head
            fi
            echo ""
        fi
 
        if [[ $task == task08 ]]
        then
            IF=hw0408.csv
            studentScript=$(awk -f $assignment-$task-$asurite* $IF 2> /dev/null)
            sdevilScript=$(cat $assignment-$task-sdevil*)
            echo ""
            if [ "$studentScript" = "$sdevilScript" ]
            then
                echo "The output is correct"
                awk -f $assignment-$task-$asurite* $IF | head
            else
                echo "$(tput bold; tput setaf 1) Incorrect Output $(tput sgr0)"
                awk -f $assignment-$task-$asurite* $IF | head
            fi
            echo ""
        fi
 
        if [[ $task == task[10][09] ]]
        then
            IF=hw0409.csv
            studentScript=$(awk -f $assignment-$task-$asurite* $IF 2> /dev/null)
            sdevilScript=$(cat $assignment-$task-sdevil*)
            echo ""
            if [ "$studentScript" = "$sdevilScript" ]
            then
                echo "The output is correct"
                awk -f $assignment-$task-$asurite* $IF | head
            else
                echo "$(tput bold; tput setaf 1) Incorrect Output $(tput sgr0)"
                awk -f $assignment-$task-$asurite* $IF | head
            fi
            echo ""
        fi
 
    done
    read -p "You are viewing $student. Press enter to view the next student"
done
