#!/bin/bash



for student in $(ls | grep "[a-Z0-9\-]*.awk$" | cut -d'-' -f3 | cut -d'.' -f1 | sort -u)
do
    clear
    for tasknum in {01..05}
    do
        assignment=lab04
        task="task$tasknum"
        asurite=$student
        echo "#################################"
        echo "# $assignment $task $asurite"
        echo "#################################"
        echo "Script Contents:"
        echo ""
        if [ -f $assignment-$task-$asurite* ]
        then
            cat *$assignment-$task-$asurite*
            echo ""
        else
            echo "$(tput bold; tput setaf 1) No submission $(tput sgr0)"
            echo ""
            continue
        fi

        echo "Script output:"
        if [[ $task == task01 ]]
        then
            IF=lab0401.csv
            studentScript=$(awk -f $assignment-$task-$asurite* $IF 2> /dev/null)
            sdevilScript=$(cat $assignment-$task-sdevil*)
            echo ""
            if [ "$studentScript" = "$sdevilScript" ]
            then
                echo "The output is correct"
                awk -f $assignment-$task-$asurite* $IF | head
            else
                echo "$(tput bold; tput setaf 1) Incorrect Output $(tput sgr0)"
                awk -f $assignment-$task-$asurite* $IF > file 
                pr -mt file $assignment-$task-sdevil.awk
            fi
            echo ""
        fi

        if [[ $task == task0[2-5] ]]
        then
            IF=lab0402.csv
            studentScript=$(awk -f $assignment-$task-$asurite* $IF 2> /dev/null)
            sdevilScript=$(cat $assignment-$task-sdevil.awk)
            echo ""
            if [ "$studentScript" = "$sdevilScript" ] || [ "$studentScript" = "$sdevilScript2" ]
            then
                echo "The output is correct"
                awk -f $assignment-$task-$asurite* $IF | head
            else
                echo "$(tput bold; tput setaf 1) Incorrect Output $(tput sgr0)"
                awk -f $assignment-$task-$asurite* $IF > file 
                pr -mt file $assignment-$task-sdevil.awk
            fi
            echo ""
        fi
 
    done
    read -p "You are viewing $student. Press enter to view the next student"
done
