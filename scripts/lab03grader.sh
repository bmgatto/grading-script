#!/bin/bash



for student in $(ls | grep "[a-Z0-9\-]*.sh$" | cut -d'-' -f3 | cut -d'.' -f1 | sort -u)
do
    clear
    for file in $(ls *$student*)
    do
        title=$(echo $file | grep -E "[hl][wa]b?[0-9][0-9].*")
        assignment=$(echo $title | cut -d"-" -f1)
        task=$(echo $title | cut -d"-" -f2)
        asurite=$(echo $title | cut -d"-" -f3 | cut -d"." -f1) 
        echo "#################################"
        echo "# $assignment $task $asurite $late"
        echo "#################################"
        echo "Script Contents:"
        echo ""
        cat $assignment-$task-$asurite*
        echo ""

        echo "Script output:"
        if [[ $task ==  "task01" ]]
        then
            echo ""
            echo -e "ExampleName\n2026/08/03\nGlendale, California\n117\nMr. Feathers\n16459" | bash $assignment-$task-$asurite* | head
            echo ""
            echo -e "YourName\n2021/09/16\nMesa, AZ\n70\nMrs. FooF\n123456" | bash $assignment-$task-$asurite* | head
            echo ""
        fi
        
        if [[ $task ==  "task02" ]]
        then
            echo ""
            echo -e "55\n15" | bash $assignment-$task-$asurite* | head
            echo -e "10\n50" | bash $assignment-$task-$asurite* | head
            echo -e "1\n1" | bash $assignment-$task-$asurite* | head
            echo ""
        fi
        
        if [[ $task ==  "task03" ]]
        then
            echo ""
            echo -e "add\n10\n5" | bash $assignment-$task-$asurite* | head
            echo -e "sub\n10\n5" | bash $assignment-$task-$asurite* | head
            echo -e "mul\n10\n5" | bash $assignment-$task-$asurite* | head
            echo -e "exp\n10\n5" | bash $assignment-$task-$asurite* | head
            echo -e "div\n10\n5" | bash $assignment-$task-$asurite* | head
            echo -e "mod\n15\n2" | bash $assignment-$task-$asurite* | head
            echo ""
        fi
        
        if [[ $task ==  "task04" ]]
        then
            echo ""
            bash $assignment-$task-$asurite* 1 2 3 4 | head
            bash $assignment-$task-$asurite* 10 20 30 40 | head
            bash $assignment-$task-$asurite* 9 | head
            echo ""
        fi
        
        if [[ $task ==  "task05" ]]
        then
            echo ""
            bash $assignment-$task-$asurite* add 10 5 | head
            bash $assignment-$task-$asurite* sub 10 5 | head
            bash $assignment-$task-$asurite* mul 10 5 | head
            bash $assignment-$task-$asurite* exp 10 5 | head
            bash $assignment-$task-$asurite* div 10 5 | head
            bash $assignment-$task-$asurite* mod 15 2 | head
            echo ""
        fi
        
    done
    read -p "You are viewing $student. Press enter to view the next student"
done
