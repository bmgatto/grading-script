#!/bin/bash



for student in $(ls | grep "[a-Z0-9\-]*.sh$" | cut -d'-' -f3 | cut -d'.' -f1 | sort -u)
do
    clear
    for tasknum in {01..10}
    do
        assignment=hw03
        task="task$tasknum"
        asurite=$student
        echo "#################################"
        echo "# $assignment $task $asurite"
        echo "#################################"
        echo "Script Contents:"
        echo ""
        cat $assignment-$task-$asurite*
        echo ""

        echo "Script output:"
        if [[ $task ==  "task01" ]]
        then
            echo ""
            bash $assignment-$task-$asurite* Are you feeling it now | head
            echo ""
        fi
        
        if [[ $task ==  "task02" ]]
        then
            echo ""
            bash $assignment-$task-$asurite* | head
            echo ""
        fi
        
        if [[ $task ==  "task03" ]]
        then
            echo ""
            echo "ExampleName" | bash $assignment-$task-$asurite* | head
            echo ""
        fi
        
        if [[ $task ==  "task04" ]]
        then
            echo ""
            bash $assignment-$task-$asurite* 1 2 3 4 5 | head
            echo ""
        fi
        
        if [[ $task ==  "task05" ]]
        then
            echo ""
            echo "Yes" | bash $assignment-$task-$asurite* | head
            echo "No" | bash $assignment-$task-$asurite* | head
            echo ""
        fi
        
        if [[ $task ==  "task06" ]]
        then
            echo ""
            echo -e "12\n5" | bash $assignment-$task-$asurite* | head
            echo ""
        fi
        
        if [[ $task ==  "task07" ]]
        then
            echo ""
            bash $assignment-$task-$asurite* | wc -l
            echo ""
        fi
               
        if [[ $task ==  "task08" ]]
        then
            echo ""
            echo -e "Instruction\nClassroom\nBanana" | bash $assignment-$task-$asurite* | head
            echo ""
        fi
        
        if [[ $task ==  "task09" ]]
        then
            echo ""
            echo "data" | bash $assignment-$task-$asurite* data | head
            echo "processor" | bash $assignment-$task-$asurite* processor | head
            echo "banana" | bash $assignment-$task-$asurite* banana | head
            echo ""
        fi

        if [[ $task ==  "task10" ]]
        then
            echo ""
            #bash $assignment-$task-$asurite* 
            echo ""
        fi

    done
    read -p "You are viewing $student. Press enter to view the next student"
done
