#!/bin/bash

function run_script {
#    for input_num in $(ls input_$tasknum* | cut -d"." -f2)
#    do
#    python3 $script < input_$tasknum.$input_num 2> /dev/null 1> tmpfile
#    student_output=tmpfile
#    solution_output=solution_$tasknum.$input_num
#    echo ""
#    if cmp -s $student_output $solution_output
#    then
#        echo "The output is correct"
#        head $student_output
#    else
#        echo "$(tput bold; tput setaf 1) Incorrect Output $(tput sgr0)"
#        pr -mt $student_output $solution_output
#    fi
#    echo ""
#    done


    echo ""
    echo -e "$1" | python3 $script | head
    echo ""

}


ASSIGNMENT=hw06
NUMTASKS=10
EXT=py

student_list=$(ls | grep "[a-Z0-9\-]*.$EXT$" | cut -d'-' -f3 | cut -d'.' -f1 | sort -u)


if [[ $1 != '' ]]
then
    student_list=$1
fi


for student in $student_list
do
    clear
    for tasknum in $(seq -w 01 $NUMTASKS)
    do
        task="task$tasknum"
        script=$ASSIGNMENT-$task-$student.$EXT
        echo "#################################"
        echo "# $script $ASSIGNMENT $task $student"
        echo "#################################"
        echo "Script Contents:"
        echo ""
        if [ -f $script ]
        then
            cat $script
            echo ""
        else
            echo "$(tput bold; tput setaf 1) No submission $(tput sgr0)"
            echo ""
            continue
        fi

        echo "Script output:"
        if [[ $task == task01 ]]
        then
            run_script "Shaun, Sammy, Brandon, Arielle, Erik, Steven\n89, 97, 47, 99, 69, 94"
        fi

        echo "Script output:"
        if [[ $task == task02 ]]
        then
            run_script "Johnson"
            run_script "Gatto"
        fi

        if [[ $task == task03 ]]
        then
            run_script "blue, green\nbeans, bird"
            run_script "black, brown\nbunny, chicken"
        fi

        if [[ $task == task04 ]]
        then
            run_script "bear, cat, elephant, fossa, tapir"
        fi


        if [[ $task == task05 ]]
        then
            run_script "dog, cat, banana, bear, fossa, apple, tapir"
        fi


        if [[ $task == task06 ]]
        then
            run_script "12.89"
            run_script "14.85"
            run_script "25.13"
        fi


        if [[ $task == task07 ]]
        then
            run_script "9.76"
            run_script "10.23"
        fi

        if [[ $task == task08 ]]
        then
            run_script "20.00"
            run_script "16.48"
            run_script "10.70"
        fi

        if [[ $task == task09 ]]
        then
            run_script "13.37\n1, .20, .10, .05, .01" 
        fi

        if [[ $task == task10 ]]
        then
            run_script "17.61\n15.25\n1, .35, .23, .12, .03, .01" 
            run_script "7.88\n20.00\n1, .35, .23, .12, .03, .01" 
            run_script "7.88\nquit"
        fi

    done
    read -p "You are viewing $student. Press enter to view the next student"
done
