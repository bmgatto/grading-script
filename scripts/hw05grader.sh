#!/bin/bash

function run_script {
#    for input_num in $(ls input_$tasknum* | cut -d"." -f2)
#    do
#    python3 $script < input_$tasknum.$input_num 2> /dev/null 1> tmpfile
#    student_output=tmpfile
#    solution_output=solution_$tasknum.$input_num
#    echo ""
#    if cmp -s $student_output $solution_output
#    then
#        echo "The output is correct"
#        head $student_output
#    else
#        echo "$(tput bold; tput setaf 1) Incorrect Output $(tput sgr0)"
#        pr -mt $student_output $solution_output
#    fi
#    echo ""
#    done


    echo ""
    echo -e "$1" | python3 $script | head
    echo ""

}


ASSIGNMENT=hw05
NUMTASKS=10
EXT=py

student_list=$(ls | grep "[a-Z0-9\-]*.$EXT$" | cut -d'-' -f3 | cut -d'.' -f1 | sort -u)


if [[ $1 != '' ]]
then
    student_list=$1
fi


for student in $student_list
do
    clear
    for tasknum in $(seq -w 01 $NUMTASKS)
    do
        task="task$tasknum"
        script=$ASSIGNMENT-$task-$student.$EXT
        echo "#################################"
        echo "# $script $ASSIGNMENT $task $student"
        echo "#################################"
        echo "Script Contents:"
        echo ""
        if [ -f $script ]
        then
            cat $script
            echo ""
        else
            echo "$(tput bold; tput setaf 1) No submission $(tput sgr0)"
            echo ""
            continue
        fi

        echo "Script output:"
        if [[ $task == task01 ]]
        then

            echo 8.8
            run_script "(1.5, -3.4)\n(4, 5)"
            echo 14.1
            run_script "(0, 0)\n(10, 10)"
            echo 5.7
            run_script "(-2, -2)\n(2, 2)"
        fi

        echo "Script output:"
        if [[ $task == task02 ]]
        then
            echo 12.57
            run_script 4
            echo 314.16
            run_script 20
            echo 7853.98 
            run_script 100
        fi

        if [[ $task == task03 ]]
        then
            run_script "Moe, Pierce, Eugene, Phil, Canberra, Bob, Fang, Scoot, Benedict, Julian"
        fi

        if [[ $task == task04 ]]
        then
            run_script 6
            run_script 6
            run_script 20
        fi


        if [[ $task == task05 ]]
        then
            run_script 2
            run_script 4
        fi


        if [[ $task == task06 ]]
        then
            run_script "Brandon"
            run_script "Erik"
        fi


        if [[ $task == task07 ]]
        then
            run_script "Todd, little caesars sign spinner, October, Minnesota, Aerobatics, grandmother"
            run_script "doop, whoop, June, AZ, wowsers, grand dad"
        fi

        if [[ $task == task08 ]]
        then
            run_script "5465" 
            run_script "60"
            run_script "3600" 
        fi

        if [[ $task == task09 ]]
        then
            run_script "24.56" 
            run_script "100"
        fi

        if [[ $task == task10 ]]
        then
            run_script "13" 
            run_script "53"
            run_script "52"
        fi

    done
    read -p "You are viewing $student. Press enter to view the next student"
done
