Pretty sure it's working now.

- Download the assignments
- Unzip the downloaded zip into a folder
- Create the files for the assignment, ie. hw0202.txt
- It helps if you set them as read only, that way students scripts can't overwrite them
- cd into the directory where you have the assignments
- Run the script

It will show you the contents of the script and output of said script for a single student. Hit enter to move to the next student.

Should work like 78% of the time.
